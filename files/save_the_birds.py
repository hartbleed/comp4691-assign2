import math
from dataclasses import dataclass
from typing import List, Tuple, Union
import numpy as np
import matplotlib.pyplot as plt
import pulp
from pulp import LpVariable as Var
from pulp import lpDot, lpSum
import random


@dataclass
class SaveTheBirdsProblem:
    name: str # name for model reasons
    time_steps: int  # number of time steps
    regions: int  # number of regions
    edges: List[Tuple[int, int]]  # edges between regions
    birds_init: List[float]  # initial birds in each region (birds)
    rats_init: List[float]  # initial rats in each region (rats)
    bird_egg_c: float  # bird egg laying "c" hyperbola parameter (eggs)
    bird_egg_s: float  # bird egg laying "s" hyperbola parameter (eggs / bird)
    bird_death_rate: float  # bird death rate (birds / bird)
    bird_migration: float  # bird migration rate (birds / bird / region)
    rat_birth_c: float  # rat birth "c" hyperbola parameter (rats)
    rat_birth_s: float  # rat birth "s" hyperbola parameter (rats / rat)
    rat_death_rate: float  # rat death rate (rats / rat)
    rat_migration: float  # rat migration rate (rats / rat / region)
    rat_hunger: float  # eggs eaten per rat (eggs / rat)
    rat_boost: float  # extra rat births per egg eaten (rats / egg)
    baiting_effectiveness: float  # fraction of rats eliminated by baiting (rats / rat)
    crews: int  # number of available crews per time step (crews)
    crews_total: int  # overall number of crews that can be deployed (crews)


@dataclass
class SaveTheBirdsSolution:
    status: str  # pulp.LpStatus[model.status]
    sol_status: str  # pulp.LpSolution[model.sol_status]
    objective: float
    birds: List[List[float]]  # birds in each time and region
    rats: List[List[float]]  # rats in each time and region
    baited: List[List[bool]]  # true if baiting conducted at time in region

# Bound calculation and helper calculations

def births(previous_pop: float, c: float, s: float) -> float:
    return (previous_pop * s * c) / (previous_pop * s + c)


def tangent(c: float, s: float, x_0: float, x: float):
    m = (((c ** 2) * s) / ((c + s * x_0) ** 2))
    b = ((c * (s ** 2) * (x_0 ** 2)) / ((c + s * x_0) ** 2))
    return m * x + b


def piecewise_births(previous_pop: float, c: float, s: float):
    interval_lower = math.floor(previous_pop)
    interval_upper = math.ceil(previous_pop)

    # Calculate current
    y1 = tangent(c, s, interval_lower, previous_pop)
    y2 = tangent(c, s, interval_upper, previous_pop)

    return min(y1, y2)

def pre_migration_bird_pop(previous_pop: [float], previous_rat_pop: [float], baited: bool,
                           prb: SaveTheBirdsProblem) -> np.ndarray:
    vector_births = np.vectorize(piecewise_births)
    eggs = vector_births(previous_pop, prb.bird_egg_c, prb.bird_egg_s)
    bait = 1
    if baited:
        bait = 0.1
    new_birds = eggs - ((previous_rat_pop * bait) / 2)
    new_birds[new_birds < 0] = 0
    return previous_pop * 0.7 + new_birds


def calculate_migration(mig_factor: float, neighbours: np.ndarray, pre_migration: np.ndarray):
    return np.sum([pre_migration[n] * mig_factor for n in neighbours])


def bird_pop_update(previous_pops: [float], previous_rat_pops: [float], baited: bool, neighbours: [[int]],
                    prb: SaveTheBirdsProblem) -> np.ndarray:
    pre_mig = pre_migration_bird_pop(previous_pops, previous_rat_pops, baited, prb)
    migration_vector = np.vectorize(calculate_migration, excluded=['pre_migration'])
    migration = migration_vector(0.02, neighbours, pre_migration=pre_mig)
    vlen = np.vectorize(len)
    return pre_mig * (1 - 0.02 * vlen(neighbours)) + migration


def pre_migration_rat_pop(previous_rat_pops: [float], baited: bool):
    # Baiting
    if baited:
        previous_rat_pops *= 0.1
    # Eating
    eggs_eaten = 0.5 * previous_rat_pops
    # Births/Deaths
    vector_births = np.vectorize(piecewise_births)
    new_rats = vector_births(previous_rat_pops, prb.rat_birth_c, prb.rat_birth_s) + 0.8 * eggs_eaten
    rat_survive = previous_rat_pops * 0.7
    return rat_survive + new_rats


def rat_pop_update(previous_rat_pop: [float], baited: bool, neighbours: [[int]]):
    pre_mig = pre_migration_rat_pop(previous_rat_pop, baited)
    # Migrate
    migration_vector = np.vectorize(calculate_migration, excluded=['pre_migration'])
    migration = migration_vector(0.05, neighbours, pre_migration=pre_mig)
    vlen = np.vectorize(len)
    return pre_mig * (1 - 0.05 * vlen(neighbours)) + migration


def get_neighbours(region: int, prb: SaveTheBirdsProblem) -> [int]:
    neighbours = set()
    for e in prb.edges:
        if region == e[0]:
            neighbours.add(e[1])
        elif region == e[1]:
            neighbours.add(e[0])
    return list(neighbours)


def upper_bound(prb: SaveTheBirdsProblem) -> float:
    previous_pops = np.array(prb.birds_init)
    previous_rat_pops = np.array(prb.rats_init)
    max_pop = 0
    min_rat_pop = float('inf')
    vget_neighbours = np.vectorize(get_neighbours, excluded=['prb'], otypes=[list])
    neighbours = vget_neighbours(region=np.arange(prb.regions), prb=prb)
    for t in range(prb.time_steps):
        new_pop = bird_pop_update(previous_pops, previous_rat_pops, True, neighbours, prb)
        new_rat_pop = rat_pop_update(previous_rat_pops, True, neighbours)
        max_bird = max(new_pop)
        min_rat = min(new_rat_pop)
        if max_bird > max_pop:
            max_pop = max_bird
        if min_rat < min_rat_pop:
            min_rat_pop = min_rat
        previous_pops = new_pop
        previous_rat_pops = new_rat_pop
    return max_pop, min_rat_pop


def lower_bound(prb: SaveTheBirdsProblem) -> float:
    previous_pops = np.array(prb.birds_init)
    previous_rat_pops = np.array(prb.rats_init)
    min_pop = float('inf')
    max_rat_pop = 0
    vget_neighbours = np.vectorize(get_neighbours, excluded=['prb'], otypes=[list])
    neighbours = vget_neighbours(region=np.arange(prb.regions), prb=prb)
    for t in range(prb.time_steps):
        new_pop = bird_pop_update(previous_pops, previous_rat_pops, False, neighbours, prb)
        new_rat_pop = rat_pop_update(previous_rat_pops, False, neighbours)
        min_bird = min(new_pop)
        max_rat = max(new_rat_pop)
        if min_pop > min_bird:
            min_pop = min_bird
        if max_rat_pop < max_rat:
            max_rat_pop = max_rat
        previous_pops = new_pop
        previous_rat_pops = new_rat_pop
    return min_pop, max_rat_pop

def all_upper_bounds(prb: SaveTheBirdsProblem) -> float:
    previous_pops = np.array(prb.birds_init)
    previous_rat_pops = np.array(prb.rats_init)
    max_pop = []
    min_rat_pop = []
    vget_neighbours = np.vectorize(get_neighbours, excluded=['prb'], otypes=[list])
    neighbours = vget_neighbours(region=np.arange(prb.regions), prb=prb)
    for t in range(prb.time_steps):
        new_pop = bird_pop_update(previous_pops, previous_rat_pops, True, neighbours, prb)
        new_rat_pop = rat_pop_update(previous_rat_pops, True, neighbours)
        max_bird = max(new_pop)
        min_rat = min(new_rat_pop)
        max_pop.append(max_bird)
        min_rat_pop.append(min_rat)
        previous_pops = new_pop
        previous_rat_pops = new_rat_pop
    return max_pop, min_rat_pop

def all_lower_bounds(prb: SaveTheBirdsProblem) -> float:
    previous_pops = np.array(prb.birds_init)
    previous_rat_pops = np.array(prb.rats_init)
    min_pop = []
    max_rat_pop = []
    vget_neighbours = np.vectorize(get_neighbours, excluded=['prb'], otypes=[list])
    neighbours = vget_neighbours(region=np.arange(prb.regions), prb=prb)
    for t in range(prb.time_steps):
        new_pop = bird_pop_update(previous_pops, previous_rat_pops, False, neighbours, prb)
        new_rat_pop = rat_pop_update(previous_rat_pops, False, neighbours)
        min_bird = min(new_pop)
        max_rat = max(new_rat_pop)
        min_pop.append(min_bird)
        max_rat_pop.append(max_rat)
        previous_pops = new_pop
        previous_rat_pops = new_rat_pop
    return min_pop, max_rat_pop

def get_bounds(prb: SaveTheBirdsProblem):
    upper_bird, lower_rat = upper_bound(prb)
    lower_bird, upper_rat = lower_bound(prb)
    return {"bird": [lower_bird, upper_bird], "rat": [lower_rat, upper_rat]}

def get_all_bounds(prb: SaveTheBirdsProblem):
    upper_birds, lower_rats = all_upper_bounds(prb)
    lower_birds, upper_rats = all_lower_bounds(prb)
    return {"bird": [lower_birds, upper_birds], "rat": [lower_rats, upper_rats]}

# SOLVING FUNCTIONS

def piecewise_x(c, s, x_0, x_1):
    # part_1 = - (s * ((1 + x_0) ** 2)) / c
    # part_2 = ((1 + 2 * x_0) * ((c + s * (1 + x_0)) ** 2)) / (c * (2 * c + s * (1 + 2 * x_0)))
    part_1 = -c*x_0 - c*x_1 - 2*s*x_0*x_1
    part_2 = 2*c + s*x_0 + s*x_1
    return -(part_1/part_2)


def piecewise_y(c, s, x_0, x_1):
    return ((c * s * (x_1 + x_0)) / (2 * c + s * (x_1 + x_0)))


def find_piecewise_points(bounds: [float], c, s):
    x = [0]
    y = [0]
    step = 5
    for x_0 in np.arange(math.floor(bounds[0]), math.ceil(bounds[1]), step):
        x.append(piecewise_x(c, s, x_0, x_0 + step))
        y.append(piecewise_y(c, s, x_0, x_0 + step))
    return x, y


def piecewise_births_variables(previous_pop: Var, new_births: Var, c: float, s: float, bounds: [float]):
    constraints_ret = []
    points_x, points_y = find_piecewise_points(bounds, c, s)

    # Attempt to optimise by skipping all the constraints if this is the first step bc we know the value
    if type(previous_pop) == float:
        constraints_ret.append(new_births == piecewise_births(previous_pop, c, s))
        return constraints_ret

    num_points = len(points_x)
    w_vars = [Var(f'w_{new_births.name}_{p}', lowBound=0, upBound=1) for p in range(num_points)]

    constraints_ret.append(previous_pop - lpDot(points_x, w_vars) == 0.0)
    constraints_ret.append(new_births - lpDot(points_y, w_vars) == 0.0)
    constraints_ret.append(lpSum(w_vars) - 1 == 0.0)

    u_vars = [Var(f'u_{new_births.name}_{p}', cat='Binary') for p in range(num_points - 1)]

    constraints_ret.append(lpSum(u_vars) - 1 == 0.0)
    for k in range(num_points):
        if k == 0:
            constraints_ret.append(w_vars[k] <= u_vars[k])
        elif k == num_points - 1:
            constraints_ret.append(w_vars[k] <= u_vars[k - 1])
        else:
            constraints_ret.append(w_vars[k] <= u_vars[k - 1] + u_vars[k])
    return constraints_ret

def pre_migration_pops(previous_bird_pop: Var, previous_rat_pop: Var, baited: Var, timestep: int, bounds: [float], region: int,
                      prb: SaveTheBirdsProblem):
    constraints_ret = []
    # Create a variable for the rat population after baiting
    after_baiting = Var(f'bait_rat_{timestep}_{region}')
    # Use on-off bait variables.
    # Idea:
    # IF baited at time step t -> baited = 1 -> 0 <= after_baiting - previous_rat_pop * 0.1 <= 0 -> after_baiting = previous_rat_pop * 0.1
    # IF not baited time step t -> baited = 0 -> 0 <= after_baiting - previous_rat_pop <= 0 -> after_baiting = previous_rat_pop
    constraints_ret.append(after_baiting - previous_rat_pop <= (bounds['rat'][1] - bounds['rat'][0]) * (baited))
    constraints_ret.append((bounds['rat'][0] - bounds['rat'][1]) * (baited) <= after_baiting - previous_rat_pop)
    constraints_ret.append(after_baiting - (previous_rat_pop * 0.1) <= (bounds['rat'][1] - bounds['rat'][0]) * (1 - baited))
    constraints_ret.append((bounds['rat'][0] - bounds['rat'][1]) * (1 - baited) <= after_baiting - (previous_rat_pop * 0.1))
    # Create a variable for birds born
    eggs_born = Var(f'eggs_born_{timestep}_{region}')
    # Calculate the number of eggs born with the piecewise function
    constraints_ret.extend(piecewise_births_variables(previous_bird_pop, eggs_born, prb.bird_egg_c, prb.bird_egg_s, bounds['bird']))
    
    # Eggs eaten by rats as a maximisation constraint
    birds_hatched = Var(f'birds_hatched_{timestep}_{region}')
    indicator_birds_hatched = [Var(f'u_{j}_eggs_eaten_{timestep}_{region}', cat='Binary') for j in range(2)]
    x = [eggs_born - after_baiting * 0.5, 0]
    for j in range(2):
        constraints_ret.append(birds_hatched >= x[j])
        constraints_ret.append(birds_hatched <= x[j] + (bounds['bird'][1] - bounds['bird'][0]) * (1 - indicator_birds_hatched[j]))
    constraints_ret.append(lpSum(indicator_birds_hatched) == 1)

    
    # Create a variable for the number of rats born
    birth_rat_var = Var(f'rats_born_{timestep}_{region}')
    # Calculate the number of rats born based on piecewise function
    constraints_ret.extend(piecewise_births_variables(after_baiting, birth_rat_var, prb.rat_birth_c, prb.rat_birth_s, bounds['rat']))
    
    return constraints_ret, birth_rat_var + 0.8 * (eggs_born - birds_hatched) + 0.7 * after_baiting, birds_hatched + 0.7 * previous_bird_pop

def time_step_constraint(previous_bird_pops: [Var], previous_rat_pops: [Var], new_bird_pops: [Var], new_rat_pops: [Var],
                         baited: [Var], timestep, bounds, neighbours: [[int]], prb: SaveTheBirdsProblem, model):
    constraints_ret = []
    # Calculate pre migration bird population
    pre_migration_expr_tuples = [pre_migration_pops(previous_bird_pops[region], previous_rat_pops[region], baited[region], timestep, bounds, region, prb) for region in range(prb.regions)]
    to_extend, pre_migration_rat_expr, pre_migration_bird_expr = zip(*pre_migration_expr_tuples)
    constraints_ret.extend([x for sublist in to_extend for x in sublist])


    # Now do migration
    for region in range(prb.regions):
        neighbouring_reg = neighbours[region]
        constraints_ret.append(new_bird_pops[region] == pre_migration_bird_expr[region] * (
                    1 - (prb.bird_migration * len(neighbouring_reg))) + lpSum([pre_migration_bird_expr[n] * prb.bird_migration for n in neighbouring_reg]))
        constraints_ret.append(new_rat_pops[region] == pre_migration_rat_expr[region] * (1 - (prb.rat_migration * len(neighbouring_reg))) + lpSum(
            [pre_migration_rat_expr[n] * prb.rat_migration for n in neighbouring_reg]))
    return constraints_ret


def save_the_bird_solver(prb: SaveTheBirdsProblem,
                         time_limit: float = 90.0,  # solver time limit
                         error: float = 3.0,  # allowed hyperbola error
                         ) -> SaveTheBirdsSolution:
    m = pulp.LpProblem(name=prb.name)

    bounds = get_bounds(prb)
    timestep_bounds = get_all_bounds(prb)

    print(bounds, timestep_bounds)

    birds = [[Var(f'bird_pop_region_{r}_time_{t}', lowBound=timestep_bounds['bird'][0][t], upBound=timestep_bounds['bird'][1][t]) for r in range(prb.regions)] for t in
             range(prb.time_steps)]
    rats = [[Var(f'rat_pop_region_{r}_time_{t}', lowBound=timestep_bounds['rat'][0][t], upBound=timestep_bounds['rat'][1][t]) for r in range(prb.regions)] for t in
            range(prb.time_steps)]
    bait = [[Var(f'baited_region_{r}_time_{t}', cat='Binary') for r in range(prb.regions)] for t in
            range(prb.time_steps)]
    neighbours = [get_neighbours(region, prb) for region in range(prb.regions)]

    constraints_all = []

    for timestep in range(prb.time_steps):
        birds_at = birds[timestep]
        rats_at = rats[timestep]
        baited_at = bait[timestep]
        if timestep == 0:
            previous_birds = prb.birds_init
            previous_rats = prb.rats_init
        else:
            previous_birds = birds[timestep - 1]
            previous_rats = rats[timestep - 1]
        local_bounds = {"bird": [timestep_bounds['bird'][0][timestep], timestep_bounds['bird'][1][timestep]], "rat": [timestep_bounds['rat'][0][timestep], timestep_bounds['rat'][1][timestep]]}
        constraints = time_step_constraint(previous_birds, previous_rats, birds_at, rats_at, baited_at, timestep, bounds, neighbours, prb, m)
        constraints_all.append(constraints)
        for c in constraints:
            m += c
        m += lpSum(baited_at) == prb.crews

    m += -lpSum(birds[-1])

    assert m.checkDuplicateVars()
    solver = pulp.PULP_CBC_CMD(mip=True, keepFiles=True, warmStart=True, timeLimit=time_limit, msg=True)
    if prb.name != "small":
        for b_t in bait:
            set = prb.crews
            for b in b_t:
                if set > 0:
                    b.setInitialValue(1)
                    set -= 1
                else:
                    b.setInitialValue(0)
                b.fixValue()

        # m.solve(pulp.PULP_CBC_CMD(mip=True, timeLimit=time_limit))

        m.solve(solver)

        print([[bird.value() for bird in bird_t]
                   for bird_t in birds])
        print([[rat.value() for rat in rat_r]
                  for rat_r in rats])
        print([[bait.value() for bait in bait_r]
                    for bait_r in bait])

        for b_t in bait:
            for b in b_t:
                b.unfixValue()
        # for bird in birds[-1]:
        #     bird.unfixValue()

    m.solve(solver)

    m.writeLP(prb.name)

    # for k, c in m.constraints.items():
    #     if not c.valid(0):
    #         print(k, c.value())
    #
    # for v in m.variables():
    #     print(v, v.value())

    # for t in range(prb.time_steps):
    #     for r in range(prb.regions):
    #         print(f"births_bird_{t}_{r}")
    #         print(m.variablesDict().get(f"births_bird_{t}_{r}").value())

    return SaveTheBirdsSolution(
        status=pulp.LpStatus[m.status],
        sol_status=pulp.LpSolution[m.sol_status],
        objective=m.objective.value(),
        birds=[[bird.value() for bird in bird_t]
               for bird_t in birds],  # REPLACE
        rats=[[rat.value() for rat in rat_r]
              for rat_r in rats],  # REPLACE
        baited=[[bait.value() for bait in bait_r]
                for bait_r in bait],  # REPLACE
    )


INSTANCE_SMALL = SaveTheBirdsProblem(
    name="small",
    time_steps=10,
    regions=1,
    edges=[],
    birds_init=[30.0],
    rats_init=[10.0],
    bird_egg_c=20.0,
    bird_egg_s=4.0,
    bird_death_rate=0.3,
    bird_migration=0.02,
    rat_birth_c=10.0,
    rat_birth_s=3.0,
    rat_death_rate=0.3,
    rat_migration=0.05,
    rat_hunger=0.5,
    rat_boost=0.8,
    baiting_effectiveness=0.9,
    crews=1,
    crews_total=3,
)

INSTANCE_MEDIUM = SaveTheBirdsProblem(
    name="medium",
    time_steps=10,
    regions=3,
    edges=[(0, 1), (1, 2)],
    birds_init=[30.0, 10.0, 1.0],
    rats_init=[10.0, 0.0, 10.0],
    bird_egg_c=20.0,
    bird_egg_s=4.0,
    bird_death_rate=0.3,
    bird_migration=0.02,
    rat_birth_c=10.0,
    rat_birth_s=3.0,
    rat_death_rate=0.3,
    rat_migration=0.05,
    rat_hunger=0.5,
    rat_boost=0.8,
    baiting_effectiveness=0.9,
    crews=2,
    crews_total=10,
)

INSTANCE_LARGE = SaveTheBirdsProblem(
    name="large",
    time_steps=5,
    regions=15,
    edges=[(0, 1), (0, 3), (1, 3), (1, 2), (1, 4), (2, 4), (3, 5), (4, 6),
           (4, 7), (5, 8), (6, 7), (6, 9), (7, 10), (8, 11), (9, 10), (9, 12),
           (10, 13), (11, 12), (11, 14), (12, 13), (13, 14)],
    birds_init=[2.4, 11.7, 23.4, 4.1, 28.6, 0.3, 5.5, 5.1, 29.9, 28.6, 28.5,
                12.1, 24.5, 19.6, 26.4],
    rats_init=[2.7, 3.5, 8.0, 6.4, 4.4, 2.8, 8.4, 9.7, 6.6, 5.1, 8.9, 1.1, 1.0,
               7.9, 9.4],
    bird_egg_c=20.0,
    bird_egg_s=4.0,
    bird_death_rate=0.3,
    bird_migration=0.02,
    rat_birth_c=10.0,
    rat_birth_s=3.0,
    rat_death_rate=0.3,
    rat_migration=0.05,
    rat_hunger=0.5,
    rat_boost=0.8,
    baiting_effectiveness=0.9,
    crews=5,
    crews_total=12,
)

if __name__ == "__main__":
    prb = INSTANCE_SMALL
    prb = INSTANCE_MEDIUM
    prb = INSTANCE_LARGE
    sol = save_the_bird_solver(prb, time_limit=90.0, error=3.0)
    print(f'Status: {sol.status}, Sol Status: {sol.sol_status}, ' +
          f'Birds: {sol.objective}')
    print(sol.birds)
    print(sol.rats)
    print(sol.baited)
    # x_vals = np.linspace(0, 1, 500)
    # y_curve = [births(x, prb.bird_egg_c, prb.bird_egg_s) for x in x_vals]
    # y_approx = [piecewise_births(x, prb.bird_egg_c, prb.bird_egg_s) for x in x_vals]
    #
    # plt.plot(x_vals, y_curve, label="Curve")
    # plt.plot(x_vals, y_approx, label="Approx")
    # print(y_curve)
    # print(y_approx)
    # plt.legend()
    # plt.savefig("approximation_close.pdf")
    # plt.show()
